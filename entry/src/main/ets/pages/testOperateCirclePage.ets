/**
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SVGManager } from '@ohos/XmlGraphicsBatik';
import { SVGCircle } from '@ohos/XmlGraphicsBatik';
import { SVGSpecifiedFormat } from '@ohos/XmlGraphicsBatik';
import { consoleInfo } from '@ohos/XmlGraphicsBatik';
import { SVGAttrConstants } from '@ohos/XmlGraphicsBatik';
import fileio from '@ohos.fileio';
import { GlobalContext } from '@ohos/XmlGraphicsBatik';
import {isArray}from '../tools/IsArrayFunction'

@Entry
@Component
struct Index {
  private svgManager: SVGManager = SVGManager.getInstance();
  private allAttrCircleObj: object = new Object();
  @State
  private svgUri: string= '';
  @State
  private filePath: string= '';

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Image(this.svgUri)
        .width(250)
        .height(250)
        .backgroundColor(Color.Pink)

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('add circle1')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            let circle: SVGCircle = new SVGCircle();
            circle.setCX(80);
            circle.setCY(80);
            circle.setR(70);
            circle.addAttribute('style', 'fill:rgb(0,0,255);stroke-width:2;stroke:rgb(0,0,0)')
            let circleObj = circle.toObj();

            let svgSpecifiedFormat: SVGSpecifiedFormat = new SVGSpecifiedFormat();
            svgSpecifiedFormat.setElementType(SVGAttrConstants.ATTR_VALUE_ELEMENT);
            svgSpecifiedFormat.setElementName('circle');
            svgSpecifiedFormat.setAttributes(circleObj);

            let svgRoot = this.svgManager.getSVGRoot();
            if (svgRoot) {
              this.allAttrCircleObj = svgSpecifiedFormat.toObj();
              this.svgManager.addChildNode(svgRoot, this.allAttrCircleObj);
              consoleInfo('Test circle: add circle1 svgTotalRoot', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })

        Button('add circle2')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            let circle: SVGCircle = new SVGCircle();
            circle.setCX(170);
            circle.setCY(80);
            circle.setR(50);
            circle.addAttribute('style', 'fill:rgb(255,0,0);stroke-width:5;stroke:rgb(0,0,255)')
            let circleObj = circle.toObj();

            let svgSpecifiedFormat: SVGSpecifiedFormat = new SVGSpecifiedFormat();
            svgSpecifiedFormat.setElementType(SVGAttrConstants.ATTR_VALUE_ELEMENT);
            svgSpecifiedFormat.setElementName('circle');
            svgSpecifiedFormat.setAttributes(circleObj);

            let svgRoot = this.svgManager.getSVGRoot();
            if (svgRoot) {
              this.svgManager.addChildNode(svgRoot, svgSpecifiedFormat.toObj());
              consoleInfo('Test circle: add circle2 svgTotalRoot', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })
      }
      .width('100%')
      .height('50')

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('update attr for circle1')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            let svgRoot = this.svgManager.getSVGRoot();
            if (!svgRoot) {
              consoleInfo('Test circle: update attr for circle1', 'svg tag is null');
              return false;
            }

            let svgElements:object = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS)as object;
            if (!svgElements) {
              consoleInfo('Test circle: update attr for circle1', `svg tag's elements is null`);
              return false;
            }

            if (typeof svgElements !== SVGAttrConstants.TYPEOF_OBJECT || !isArray(svgElements)) {
              consoleInfo('Test circle: update attr for circle1', `the elements's type of svg tag is not array`);
              return;
            }

            let circleResult:Record<string,object> = {};
            try {
              svgElements.forEach((item:object) => {
                if (typeof item === SVGAttrConstants.TYPEOF_OBJECT) {
                  let nameValue: string = this.svgManager.getValueForKey(item, SVGAttrConstants.ATTR_KEY_NAME) as string;
                  if (nameValue === 'circle') {
                    circleResult = item as Record<string, object>;
                    throw new Error('has got circle,jump out');
                  }
                }
              })
            } catch (e) {
              if (!circleResult) {
                consoleInfo('Test circle: update attr for circle1', 'circle not exist');
                return;
              }

              if (typeof circleResult === SVGAttrConstants.TYPEOF_OBJECT) {
                let circleAttributes:Record<string,number> = circleResult[SVGAttrConstants.ATTR_KEY_ATTRIBUTES]as Record<string,number>;
                circleAttributes['cx'] = 70;
                circleAttributes['cy'] = 50;
                circleAttributes['r'] = 20;
                this.svgManager.setAttribute(circleAttributes, 'style', 'fill:rgb(0,255,0);stroke-width:10;stroke:rgb(0,255,255)');
                this.allAttrCircleObj = circleResult;
              }
              consoleInfo('Test circle: update attr for circle1 svgTotalObj', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
            return
          })

        Button('remove circle2')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            let svgRoot = this.svgManager.getSVGRoot();
            if (!svgRoot) {
              consoleInfo('Test circle: remove circle2', 'svg tag is null');
              return;
            }

            let svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);
            if (!svgElements) {
              consoleInfo('Test circle: remove circle2', `svg tag's elements is null`);
              return;
            }

            if (typeof svgElements !== SVGAttrConstants.TYPEOF_OBJECT || !isArray(svgElements)) {
              consoleInfo('Test circle: remove circle2', `svg tag's elements is null`);
              return;
            }

            if (svgElements.length >= 2) {
              svgElements.splice(1, 1);
            }
            consoleInfo('Test remove circle2 svgTotalObj', JSON.stringify(this.svgManager.getSVGTotalObj()));
          })
      }
      .margin(10)
      .width('100%')
      .height('50')

      Button('show svg with circle')
        .fontSize(20)
        .fontWeight(FontWeight.Bold)
        .width(250)
        .height(50)
        .onClick(() => {
          let svgTotalObj = this.svgManager.getSVGTotalObj();
          let success =  ()=> {
            consoleInfo('Test circle: saveFile', 'success');
          }

          let fileName = new Date().getTime() + 'Circle.svg'
          this.svgManager.saveSVG(fileName, svgTotalObj, success);
          setTimeout(() => {
              consoleInfo('Test circle: show svg getFilePath ', this.filePath);
              this.svgUri = 'file://' + this.filePath + '/' + fileName;
          }, 100);
        })


      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('remove cx attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('cx');
          })

        Button('remove cy attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('cy');
          })
      }
      .margin(10)
      .width('100%')
      .height('50')

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('remove r attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('r');
          })
      }
      .margin(10)
      .width('100%')
      .height('50')
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    let filesDir: string = GlobalContext.getContext().getObject("filesDir") as string
    this.filePath = filesDir
    // 清空已存在的SVG根
    this.svgManager.createSVGDeclares();
  }

  removeAttribute(firstAttrName: string, secondAttrName?: string) {
    if (!this.allAttrCircleObj) {
      consoleInfo('test remove ' + firstAttrName, 'circle is not added.');
      return;
    }
    let circleJson = JSON.stringify(this.allAttrCircleObj);
    let circleOriginData:object = JSON.parse(circleJson);

    let attrs = this.svgManager.getValueForKey(circleOriginData, SVGAttrConstants.ATTR_KEY_ATTRIBUTES) as object;
    if (!attrs) {
      consoleInfo('test remove ' + firstAttrName, 'circle1 has no attributes');
      return;
    }
    this.svgManager.removeByKey(attrs, firstAttrName);
    if (secondAttrName) {
      this.svgManager.removeByKey(attrs, secondAttrName);
    }

    // 替换 circle
    let svgRoot = this.svgManager.getSVGRoot();
    let svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);

    if (typeof svgElements === SVGAttrConstants.TYPEOF_OBJECT && isArray(svgElements)) {
      svgElements.splice(0, 1, circleOriginData);
    }
    consoleInfo('test remove attr: ' + firstAttrName, JSON.stringify(this.svgManager.getSVGTotalObj()));
  }

  aboutToDisappear() {
    let filesDir: string = GlobalContext.getContext().getObject("filesDir") as string
    this.filePath = filesDir
    this.removeExistSVGFile();
  }

  removeExistSVGFile() {
    let dir = fileio.opendirSync(this.filePath);
    let that = this;
    dir.read()
      .then((dirent) => {
        if (dirent && dirent.isFile()) {
          fileio.unlink(this.filePath + '/' + dirent.name, (removeErr) => {
            if (removeErr) {
              consoleInfo('test circle remove file', 'failed');
            }
          })
          that.removeExistSVGFile();
        }
      })
      .catch((err:string) => {
        consoleInfo('test circle remove file failed', err);
      })
  }
}