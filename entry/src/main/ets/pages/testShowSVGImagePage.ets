/**
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SVGManager, consoleInfo } from '@ohos/XmlGraphicsBatik';
import { GlobalContext } from '@ohos/XmlGraphicsBatik';

@Entry
@Component
struct Index {
  private svgManager: SVGManager = SVGManager.getInstance();
  @State
  private filePath: string= '';

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Image($r('app.media.svgSample'))
        .width(500)
        .height(500)

      Image($r('app.media.svgAnimate'))
        .width(500)
        .height(500)
        .margin({ top: 5 })

      Image('file://' + this.filePath + '/svg.svg')
        .width(150)
        .height(150)
        .margin({ top: 5 })
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    let success = () => {
      consoleInfo('Test parse svg file, saveFile', 'success');
    }
    let filesDir: string = GlobalContext.getContext().getObject("filesDir") as string
    this.filePath = filesDir
    let svgOriginXml: string = GlobalContext.getContext().getObject("svgOriginXml") as string
    this.svgManager.saveSVG('svg.svg', svgOriginXml, success);
  }
}